class Item {
    id?:string;
    sku:string;
    barcode:string;
    itemNumber:string;
    price:number;
}

export default Item;